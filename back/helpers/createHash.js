const config = require("../config");
const crypto = require("crypto");

const CreateHash = (string) => (
  crypto.createHmac('sha256', config.SECRET_KEY)
    .update(string)
    .digest('hex')
)

module.exports = CreateHash