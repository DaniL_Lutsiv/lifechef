const createVerify = (email, hash) => {
  const arr = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];

  return {
    code: arr.sort(() => Math.random() - 0.5).slice(0, 4).join(''),
    email,
    hash,
  }
}



module.exports = createVerify