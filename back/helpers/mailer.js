const config = require("../config");
const nodemailer = require('nodemailer')

module.exports = () => {
  let transporter = nodemailer.createTransport({
    service: 'Gmail', auth: {
      user: config.MAIL_USERNAME,
      pass: config.MAIL_PASSWORD,
    },
  })

  transporter.verify(function(e) {
    if (e) {
      console.error(e);
    } else {
      console.log("mailer success start");
    }
  });

  return transporter
}
