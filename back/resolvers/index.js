const database = require("../database");
const createHash = require('../helpers/createHash')
const config = require("../config");
const mailer = require("../helpers/mailer");
const createVerify = require("../helpers/createVerify");

const resolvers = {
  Query: {
    mailApply: (_, { code, hash } ) => {
      return {success: !!database.mailChangesList.find((el) => el.hash === hash && el.code === code)}
    },

    getEmail: (_, { hash }) => {
      const email = database.mailChangesList.find((el) => el.hash === hash)?.email

      return {email, success: !!email}
    }
  },

  Mutation: {
    mailChange: async (_, { email } ) => {
      const hash = createHash(email)

      const verify = createVerify(email, hash)

      try {
        await mailer().sendMail({
          from: 'danya300914@gmial.com', // sender address
          to: verify.email,
          subject: 'Change email', // Subject line
          text: `Your code: ${verify.code} || clink me ${config.URL}/verify?hash=${hash}`
        })

        database.mailChangesList.push(verify)
        return {success: true, hash}
      } catch (e) {
        console.log(e)
        return { success: false, error: e}
      }
    }
  }
};


module.exports = resolvers;