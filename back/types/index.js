const { gql } = require('apollo-server');

const typeDefs = gql`
  type Vefify {
    email: String!
    code: String!
  }
  
  type Email {
      email: String  
      success: Boolean!
  }
  
  type Response {
      success: Boolean!
      hash: String!
  }
  
  type Query {
      mailApply(hash: String!, code: String!): Response!
      getEmail(hash: String!): Email!
  }

  type Mutation {
      mailChange(email: String!): Response!
  }
`;

module.exports = typeDefs;