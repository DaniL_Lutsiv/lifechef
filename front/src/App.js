import './App.css'
import {Route} from 'react-router-dom'
import MainPage from "./pages/MainPage";
import VerifyPage from "./pages/VerifyPage";
import Header from "./components/Header";
import {ContentWrapper} from "./styled";
import ContactUs from "./components/ContactUs";
import {FormProvider} from "./providers/FormProvider";
import {client} from "./api/config";
import {ApolloProvider} from "@apollo/client";


function App() {
  return (
    <ApolloProvider client={client}>
      <div>
        <FormProvider>
          <Header/>
          <ContentWrapper>
            <Route exact path="/" render={() => (<MainPage/>)}/>
            <Route path="/verify" render={() => (<VerifyPage/>)}/>
            <ContactUs/>
          </ContentWrapper>
        </FormProvider>
      </div>
    </ApolloProvider>
  );
}

export default App;
