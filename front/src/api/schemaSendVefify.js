import {gql} from '@apollo/client';

export const VERIFY_CODE = gql`
    mutation mailChange($email: String!) {
        mailChange(email: $email) {
            success
            hash
        }
    }
`;

export const GET_EMAIL = gql`
    query getEmail($hash: String!){
        getEmail(hash: $hash) {
            email
            success
        }
    }
`

export const MAIL_APPLY = gql`
    query mailApply($code: String!, $hash: String!) {
        mailApply(code: $code, hash: $hash) {
            success
        }
    }
`