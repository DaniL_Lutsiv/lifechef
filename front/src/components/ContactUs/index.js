import React from "react";
import {ContactUsWrapper, HeadText, Method, Methods, TextInfo} from "./styled";
import chat from '../../images/chat.png'
import mail from '../../images/mail.png'
import question from '../../images/question.png'

const ContactUs = React.memo(() => {
  return (
    <ContactUsWrapper>
      <HeadText>
        Need help?
      </HeadText>
      <Methods>
        <Method>
          <img src={chat} alt=""/>
          <div>Chat</div>
        </Method>
        <Method>
          <img src={mail} alt=""/>
          <div>Chat</div>
        </Method>
        <Method>
          <img src={question} alt=""/>
          <div>Chat</div>
        </Method>
      </Methods>
      <TextInfo>Call us: 1955 932 4048</TextInfo>
      <TextInfo>Monday – Friday, 8:00 AM to 5:00 PM EST</TextInfo>
    </ContactUsWrapper>
  )
})

export default ContactUs
