import styled from "styled-components";

export const ContactUsWrapper = styled.footer`
  max-width: 636px;
  margin: auto;
`

export const HeadText = styled.div`
  text-align: center;
  font-weight: 600;
  font-size: 20px;
  line-height: 24px;
  margin-bottom: 24px;
`

export const Methods = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-bottom: 16px;
  & > div{
    margin-right: 24px;
  }
  
  & > div:last-child {
    margin-right: 0;
  }
  
  @media (max-width: 636px) {
    flex-direction: column;
  
    align-items: center;
    & > div{
      margin-right: 0;
      margin-bottom: 24px;
    }

    & > div:last-child {
      margin-bottom: 0;
    }
  }
`

export const Method = styled.div`
  width: 180px;
  height: 108px;
  border-radius: 8px;
  background-color: #fff;
  display: flex;
  flex-direction: column;
  align-items: center;
  box-shadow: 0px 3px 7px rgba(1, 48, 36, 0.07);
  justify-content: center;
  font-weight: 600;
  cursor: pointer;
  
  
  & > img {
    margin-bottom: 17.67px;
  }
`

export const TextInfo = styled.div`
  color: #87898C;
  font-size: 12px;
  text-align: center;
  margin-top: 8px;
`
