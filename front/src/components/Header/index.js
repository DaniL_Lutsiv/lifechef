import React, {useContext} from "react";
import {HeaderWrapper, HeaderContent, ButtonWrapper, WhiteButton, GreenButton} from "./styled";
import logo from '../../images/logo.png'
import {FormContext} from "../../providers/FormProvider";

const Header = React.memo(() => {
  const {setViewFirstModel} = useContext(FormContext)

  const SignUpHandler = () => {
    setViewFirstModel(true)
  }

  return (
    <HeaderWrapper>
      <HeaderContent>
        <img src={logo} alt="logo"/>
        <ButtonWrapper>
          <WhiteButton>
            Login
          </WhiteButton>
          <GreenButton onClick={SignUpHandler}>
            Sign Up
          </GreenButton>
        </ButtonWrapper>
      </HeaderContent>
    </HeaderWrapper>
  )
})

export default Header