import styled from 'styled-components'

export const HeaderWrapper = styled.header`
  height: 80px;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  box-shadow: inset 0px -1px 0px #E1E3E6;
  background-color: #fff;
`

export const HeaderContent = styled.div`
  margin: 16px 8.57% 20px;
  display: flex;
  justify-content: space-between;
`

export const ButtonWrapper = styled.div`
  display: flex;
`

export const Button = styled.button`
  width: 89px;
  height: 40px;
  border-radius: 8px;
  outline: 0;
  border: none;
  padding: 10px 16px;
  background-color: #fff;
  cursor: pointer;
`

export const WhiteButton = styled(Button)`
  &:hover {
    background-color: #f1f1f1;
  }
`

export const GreenButton = styled(Button)`
  color: white;
  background-color: #0A6629;

  &:hover {
    background-color: #08501f;
  }
`