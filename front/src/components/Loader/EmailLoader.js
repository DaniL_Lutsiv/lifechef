import React from "react";
import {LoaderContent, EmailLoaderWrapper} from "./styled";

const EmailLoader = React.memo(() => {
  return (
    <EmailLoaderWrapper>
      <LoaderContent>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </LoaderContent>
    </EmailLoaderWrapper>
  )
})

export default EmailLoader