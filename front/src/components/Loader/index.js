import React from "react";
import {LoaderContent, LoaderWrapper} from "./styled";

const Loader = React.memo(({loader}) => {
  if (!loader) return <></>

  return (
    <LoaderWrapper>
      <LoaderContent>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </LoaderContent>
    </LoaderWrapper>
  )
})

export default Loader