import styled from "styled-components";

export const LoaderWrapper = styled.div`
  position: absolute;
  right: -28px;
  top: -4px;
`

export const EmailLoaderWrapper = styled.div`

  & > div {
    width: 40px;
    height: 40px;

    & > div {
      width: 40px;
      height: 40px;
      border: 3px solid;
      border-color: #242424 transparent transparent transparent;
    }
  }
`

export const LoaderContent = styled.div`
  display: inline-block;
  position: relative;
  width: 20px;
  height: 20px;
  margin-top: 3px;
  margin-left: 5px;
  
  & > div {
    box-sizing: border-box;
    display: block;
    position: absolute;
    width: 16px;
    height: 16px;
    border: 2px solid;
    border-radius: 50%;
    animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: #fff transparent transparent transparent;
  } 

  & > div:nth-child(1) {
    animation-delay: -0.45s;
  }

  & > div:nth-child(2) {
    animation-delay: -0.3s;
  }
  
  & > div:nth-child(3) {
    animation-delay: -0.15s;
  }
  
  @keyframes lds-ring {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`