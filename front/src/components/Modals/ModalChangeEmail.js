import React from "react";
import {
  FormButton,
  FormName,
  FormWrapper,
  Input,
  InputLabel,
  InputWrapper,
  LoaderWrapper,
  Modal,
  ModalCloseButton,
  ModalContext,
  ModalText,
  ModalWrapper
} from "./styled";
import Loader from "../Loader";

const ModalChangeEmail = React.memo(({view, setView, email, setEmail, sendEmail, invalid, loading}) => {
  if (view === null) return <></>

  return (
    <ModalWrapper view={view}>
      <Modal>
        <ModalCloseButton onClick={() => setView(false)}>
          &#10005;
        </ModalCloseButton>
        <ModalText>
          Change Email
        </ModalText>
        <ModalContext>
          <FormWrapper>
            <FormName>
              Enter new e-email
            </FormName>
            <InputWrapper>
              <InputLabel>
                New e-mail
              </InputLabel>
              <Input value={email} onChange={(e) => setEmail(e.target.value)} invalid={invalid}/>
            </InputWrapper>
            <FormButton onClick={sendEmail}>
              <LoaderWrapper>
                <div>Save</div>
                <Loader loader={loading}/>
              </LoaderWrapper>
            </FormButton>
          </FormWrapper>
        </ModalContext>
      </Modal>
    </ModalWrapper>
  )
})

export default ModalChangeEmail