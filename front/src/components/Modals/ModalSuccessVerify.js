import React from "react";
import {IconSuccess, Modal, ModalMessage, ModalText, ModalWrapper} from "./styled";
import Success from "../../images/success";

const ModalSuccessVerify = React.memo(({view}) => {
  if (view === null) return <></>
  return (
    <ModalWrapper view={view}>
      <Modal>
        <IconSuccess>
          <Success/>
        </IconSuccess>


        <ModalText>
          Your email has been changed!
        </ModalText>

        <ModalMessage>
          Please, check you inbox and follow the link in order to verify your email.
        </ModalMessage>

      </Modal>
    </ModalWrapper>
  )
})

export default ModalSuccessVerify