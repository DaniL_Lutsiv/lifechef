import styled from 'styled-components'

export const ModalWrapper = styled.div`
  background-color: #fff;
  max-width: 520px;
  border-radius: 8px;
  position: absolute;
  margin-left: auto;
  margin-right: auto;
  left: 0;
  right: 0;
  top: 200px;
  text-align: center;
  animation: ${({view}) => view ? 'showModal' : 'hideModal'} 1s;
  animation-fill-mode: both;

  @keyframes showModal {
    0% {
      opacity: 0;
      transform: scale(0);
    }
    100% {
      opacity: 1;
      transform: scale(1);
    }
  }

  @keyframes hideModal {
    0% {
      opacity: 1;
      transform: scale(1);
    }
    100% {
      opacity: 0;
      transform: scale(0);
    }
  }
`

export const ModalText = styled.div`
  font-weight: 500;
  font-size: 24px;
  line-height: 28px;
  letter-spacing: -0.3px;
`

export const Modal = styled.div`
  position: relative;
  padding: 80px 80px 16px;
`

export const ModalCloseButton = styled.button`
  position: absolute; 
  right: 21.75px;
  top: 21.75px;
  border: 0;
  outline: 0;
  background-color: #fff;
  font-size: 26px;
  font-weight: 400;
  cursor: pointer;
`
export const ModalContext = styled.div`
  width: 100%;
  text-align: left;
`

export const FormWrapper = styled.div`
  position: relative;
  width: 100%;
  margin-top: 16px;
`

export const FormName = styled.div`
  position: relative;
  width: 100%;
  font-weight: 400;
  font-size: 16px;
  line-height: 20px;
`

export const InputWrapper = styled.div`
  margin: 16px 0;
`

export const InputLabel = styled.label`
  position: absolute;
  top: 24px;
  left: 10px;
  background-color: #fff;
  font-size: 12px;
  font-weight: 500;
  line-height: 16px;
  padding: 5px;
`

export const Input = styled.input`
  width: calc(100% - 20px);
  height: 36px;
  border-radius: 8px;
  border: 1px solid ${({invalid}) => invalid ? '#ff2b2b' : '#BEC1C4'};
  outline: 0;
  font-weight: 400;
  font-size: 16px;
  line-height: 20px;
  color: #87898C;
  padding: 10px;
`
export const FormButton = styled.button`
  background-color: #0A6629;
  height: 56px;
  border-radius: 8px;
  font-size: 14px;
  text-align: center;
  width: 100%;
  color: #fff;
  cursor: pointer;
  margin-bottom: 64px;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const IconSuccess = styled.div`
  margin-top: 4px;
  margin-bottom: 20px;
  position: relative;
  
  & > svg {
    height: 40px;
    width: 40px;
  }
`

export const ModalMessage = styled.div`
  margin-top: 16px;
  font-weight: 300;
  letter-spacing: -0.1px;
  font-size: 16px;
  line-height: 24px;
  margin-bottom: 64px;
`

export const LoaderWrapper = styled.div`
  position: absolute
`