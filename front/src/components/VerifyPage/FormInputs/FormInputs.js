import React, {useEffect, useMemo, useRef, useState} from "react";
import {Inputs, Input, InputWrapper} from "./styled";

const FormInputs = ({onSubmit, valid, setValid}) => {
  const ref = useRef()
  const length = 4
  const [value, setValue] = useState('')
  const [active, setActive] = useState(0)

  useEffect(() => {
    if (value.length >= length) {
      onSubmit(value)
    }
  }, [value])

  const handler = async (i, inputValue) => {
    if (inputValue.length > 1) {
      ref.current.children[i - 1].firstChild.value = inputValue.slice(0, 1)
      if (i < length){
        setActive(i)
        ref.current.children[i].firstChild.focus()
      }
    }

    if (inputValue.length === 1){
      setValue(prev => prev + inputValue.toString())

      if (i < length){
        setActive(i)
        ref.current.children[i].firstChild.focus()
      }
    }
  }

  const keyDownEvent = (e) => {
    const { keyCode } = e;

    if (keyCode === 8 && active) {
      setValid(null)
      setValue(p => p.slice(0, p.length - 1))
      ref.current.children[active].firstChild.value = ''

      setTimeout(() => {
        ref.current.children[active - 1].firstChild.focus()
        setActive(active - 1)
      }, 0);
    }
    if (!active){
      setValue('')
      setActive(0)
    }
  }

  useEffect(() => {
    document.addEventListener('keydown', keyDownEvent)
    return () => {
      document.removeEventListener('keydown', keyDownEvent)
    }
  }, [keyDownEvent, value])


  const inputs = useMemo(() => {
    return Array.from({ length }).map((_, i) => (
      <InputWrapper key={i}>
        <Input type="number" onChange={(e) => handler(i + 1, e.target.value)} valid={valid}/>
      </InputWrapper>
    ))
  }, [onSubmit, value, valid])

  return (
    <Inputs ref={ref}>
      {inputs}
    </Inputs>
  )
}

export default FormInputs