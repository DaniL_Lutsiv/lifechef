import styled from "styled-components";

export const Inputs = styled.div`
  display: flex;

  & > div {
    margin-right: 8px;
  }
  
  & > div:last-child {
    margin-right: 0;
  }
`

export const InputWrapper = styled.div`
  &:after{
    content: "";
    height: 2px;
    background: #BEC1C4;
    border-radius: 1px;
    display: block;
  }
`

export const Input = styled.input`
  width: 48px;
  outline: 0;
  height: 60px; 
  border: 0;
  font-size: 40px;
  color: ${({valid}) => valid === null ? '#BEC1C4' : valid ? 'green' : 'red'};
  text-align: center;
  

  &::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
  }
`