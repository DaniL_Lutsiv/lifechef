import styled from "styled-components";
import {ModalMessage, ModalText} from "../Modals/styled";
import {Method} from "../ContactUs/styled";

export const VerifyPageWrapper = styled.div`
  max-width: 588px;
  margin: 40px auto 80px;
  text-align: center;
`

export const Email = styled.div`
  font-weight: 600;
  font-size: 20px;
  line-height: 24px;
  margin-bottom: 24px;
`

export const Message = styled(ModalMessage)`
  margin-bottom: 16px;
  font-size: 14px;
`

export const VerifyForm = styled(Method)`
  width: calc(100% - 160px);
  min-height: 266px;
  padding: 40px 80px;
  margin-bottom: 24px;
`
export const FormName = styled(ModalText)`
  margin-top: 0;
  margin-bottom: 8px;
`
export const Green = styled.span`
  color: #0A6629;
`

