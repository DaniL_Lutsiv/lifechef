import React, {useCallback, useContext, useEffect, useState} from "react";
import {WrapperMailPage} from "../components/MainPage/styled";
import ModalChangeEmail from "../components/Modals/ModalChangeEmail";
import ModalSuccessVerify from "../components/Modals/ModalSuccessVerify";
import {FormContext} from "../providers/FormProvider";
import {useMutation} from '@apollo/client';
import {VERIFY_CODE} from "../api/schemaSendVefify";
import {useHistory} from 'react-router-dom'

const MainPage = React.memo(() => {
  const {viewFirstModel, viewSecondModel, setViewFirstModel, setViewSecondModel} = useContext(FormContext)
  const [email, setEmail] = useState('')
  const [invalid, setInvalid] = useState(false)
  const [mailChange, {data, loading}] = useMutation(VERIFY_CODE);
  const history = useHistory()

  const sendEmail = useCallback(async () => {
    if (/[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ a\t\r\n]+/.test(email)) {
      try {
        await mailChange({variables: {email}})
      } catch (e) {
        console.error(e)
      }
    } else {
      setInvalid(true)
      setTimeout(() => setInvalid(false), 1000)
    }
  }, [email, invalid])

  useEffect(() => {
    if (data?.mailChange?.success) {
      setViewFirstModel(false)

      setTimeout(() => { // animation modal
        setViewSecondModel(true)
        setTimeout(() => {
          setViewSecondModel(false)
          setTimeout(() => {
            history.push(`/verify?hash=${data?.mailChange?.hash}`)
          }, 1000)
        }, 1600)
      }, 300)
    }
  }, [data])


  return (
    <WrapperMailPage>
      <ModalChangeEmail
        view={viewFirstModel}
        setView={setViewFirstModel}
        email={email}
        setEmail={setEmail}
        sendEmail={sendEmail} i
        invalid={invalid}
        loading={loading}/>
      <ModalSuccessVerify view={viewSecondModel}/>
    </WrapperMailPage>
  )
})

export default MainPage