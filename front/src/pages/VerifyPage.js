import React, {useEffect, useState} from "react";
import {Email, FormName, Green, Message, VerifyForm, VerifyPageWrapper} from "../components/VerifyPage/styled";
import Success from "../images/success";
import {ReactComponent as Error} from "../images/error.svg";
import {IconSuccess, ModalMessage} from "../components/Modals/styled";
import FormInputs from "../components/VerifyPage/FormInputs/FormInputs";
import {useLazyQuery, useQuery} from "@apollo/client";
import {GET_EMAIL, MAIL_APPLY} from "../api/schemaSendVefify";
import {useLocation} from "react-router-dom";
import EmailLoader from "../components/Loader/EmailLoader";


const VerifyPage = React.memo(() => {
  const [mailApply, { data }] = useLazyQuery(MAIL_APPLY);
  const hash = new URLSearchParams(useLocation().search).get('hash');
  const [success, setSuccess] = useState(null)
  const [valid, setValid] = useState(null)
  const {data: email} = useQuery(GET_EMAIL, {variables: {hash}});

  useEffect(() => !hash && setSuccess(false),[hash])

  useEffect(() => data !== undefined && setValid(data.mailApply?.success),[data])

  useEffect(() => {
    if (email?.getEmail?.success !== undefined){
      setSuccess(email?.getEmail?.success)
    }
  }, [email])


  const onSubmit = async (code) => {
    try {
      await mailApply({variables: {code, hash}})
    } catch (e) {
      console.error(e)
    }
  }


  return (
    <VerifyPageWrapper>
      <IconSuccess>
        {success === null ? <EmailLoader /> : success ? <Success/> : <Error/>}
      </IconSuccess>
      <Message>
        {success === false ? 'Incorrect link' : success === false ? 'Verification email has been sent to' : ''}
      </Message>
      <Email>
        {email?.getEmail?.email || ''}
      </Email>
      {success && (
        <VerifyForm>
          <FormName>
            Verify your account
          </FormName>

          <ModalMessage>
            Please, enter the code here or follow the link from the email. Link expires <b>in 48 hours.</b>
          </ModalMessage>

          <FormInputs onSubmit={onSubmit} valid={valid} setValid={setValid}/>
        </VerifyForm>
      )}
      <Message>
        Haven’t got an email? Check spam folder or <Green>Resend</Green>
      </Message>
    </VerifyPageWrapper>
  )
})

export default VerifyPage