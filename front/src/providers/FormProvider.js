import React, {useState} from 'react'

export const FormContext = React.createContext({})

export const FormProvider = (props) => {
  const [viewFirstModel, setViewFirstModel] = useState(true)
  const [viewSecondModel, setViewSecondModel] = useState(null)

  return (
    <FormContext.Provider
      value={{
        viewFirstModel,
        viewSecondModel,
        setViewFirstModel,
        setViewSecondModel,
      }}
    >
      {props.children}
    </FormContext.Provider>
  )
}
