import styled from 'styled-components'

export const ContentWrapper = styled.section`
  background-color: #F5F6F7;
  padding: 80px;
`